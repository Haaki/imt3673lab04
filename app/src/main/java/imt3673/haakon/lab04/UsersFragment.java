package imt3673.haakon.lab04;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class UsersFragment extends Fragment {

    private ListView userList;
    private List<String> users = new ArrayList<>();
    private int chatOption = 0;
    private String currentUser;

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference dbRef = database.getReference();
    private DatabaseReference dbUser = dbRef.child("users");
    private UserAdapter userAdapter;

    //The fragment argument representing the section number for this fragment.
    private static final String ARG_SECTION_NUMBER = "section_number";


    //Returns a new instance of this fragment for the given section number.
    public static UsersFragment newInstance(int sectionNumber) {
        UsersFragment fragment = new UsersFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_users, container, false);

        users.add("View all chats");
        userList = rootView.findViewById(R.id.userList);
        userAdapter = new UserAdapter(rootView.getContext(), users);
        userList.setAdapter(userAdapter);
        userAdapter.colors.add(0xFF9999FF);

        userList.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position, long id){
                if (chatOption != position) {
                    chatOption = position;
                    currentUser = users.get(chatOption);
                    for (int i = 0; i < userAdapter.colors.size(); i++)
                        userAdapter.colors.set(i, 0);
                    userAdapter.colors.set(position, 0xFF9999FF);
                    userAdapter.notifyDataSetChanged();


                    ChatFragment.chatOption = chatOption;
                    ChatFragment.currentChat = currentUser;
                    ChatFragment.connectToDatabase();
                    //ChatFragment.chatList.setAdapter(new ChatAdapter(getFragmentManager().getFragments().get(0).getContext(), new ArrayList<ChatFragment.ChatElement>() {}));
                    ChatFragment.chatAdapter.changeChat();
                    ChatFragment.chatAdapter = new ChatAdapter(getFragmentManager().getFragments().get(0).getContext(), ChatFragment.chatAdapter.chatElements);
                    //ChatFragment.chatList.removeAllViewsInLayout();
                    ChatFragment.chatList.setAdapter(ChatFragment.chatAdapter);
                }
            }
        });

        return rootView;
    }

    @Override
    public void onStart(){
        super.onStart();

        dbUser.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                String s = dataSnapshot.getValue(String.class);
                if (s != null) {
                    if (!isDuplicate(s)) {
                        users.add(s);
                        userAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private boolean isDuplicate(String s) {
        for (int i = 0; i < users.size(); i++) {
            if (s.matches(users.get(i)))
                return true;
        }
        return false;
    }
}

