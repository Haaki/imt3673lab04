package imt3673.haakon.lab04;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ChatAdapter extends BaseAdapter {
    List<ChatFragment.ChatElement> chatElements;
    LayoutInflater inflater;

    public ChatAdapter(Context applicationContext, List<ChatFragment.ChatElement> chatElements) {
        this.chatElements = chatElements;
        inflater = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return chatElements.size();
    }

    @Override
    public Object getItem(int i) {
        return chatElements.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public void notifyDataSetChanged() {
        changeChat();
        super.notifyDataSetChanged();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        view = inflater.inflate(R.layout.list_element, null);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(view.getContext());

        TextView user = view.findViewById(R.id.user);
        TextView message = view.findViewById(R.id.message);
        TextView date = view.findViewById(R.id.date);
        user.setText(chatElements.get(i).user);
        message.setText(chatElements.get(i).message);
        message.setTextSize(18);
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        Date tempDate = new Date();
        try {tempDate = format.parse(chatElements.get(i).date);} catch (java.text.ParseException e){}
        SimpleDateFormat newFormat = new SimpleDateFormat("HH:mm:ss - dd.MM.yyyy");
        date.setText(newFormat.format(tempDate));


        String currentUser = prefs.getString("Username","");
        String textUser = chatElements.get(i).user;
        if (currentUser.matches(textUser)){
            message.setBackgroundColor(0xFF9999FF);
            user.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
            message.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
            date.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
        }
        else{
            message.setBackgroundColor(0xFFCCCCCC);
            user.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
            message.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
            date.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
        }
        return view;
    }

    public void changeChat() {
        Collections.sort(ChatFragment.messages, new Comparator<ChatFragment.ChatElement>() {
            public int compare(ChatFragment.ChatElement ce1, ChatFragment.ChatElement ce2) {
                if (ce1.date != null && ce2.date != null)
                    return (Long.parseLong(ce1.date) < Long.parseLong(ce2.date)) ? -1 : (Long.parseLong(ce1.date) == Long.parseLong(ce2.date)) ? 0 : 1;
                else
                    return 0;
            }
        });
        this.chatElements = ChatFragment.messages;
        for (int i=0; i<this.chatElements.size(); i++) {
            if (ChatFragment.chatOption != 0 && !ChatFragment.currentChat.matches(this.chatElements.get(i).user)) {
                this.chatElements.remove(i);
                i--;
            }
            if (i>0)
                if (this.chatElements.get(i).date == this.chatElements.get(i-1).date
                        && this.chatElements.get(i).user == this.chatElements.get(i-1).user
                        && this.chatElements.get(i).message == this.chatElements.get(i-1).message){
                    this.chatElements.remove(i);
                    i--;
                }
        }
    }
}

