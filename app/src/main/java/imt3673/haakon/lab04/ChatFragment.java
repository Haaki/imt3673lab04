package imt3673.haakon.lab04;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ChatFragment extends Fragment {

    public static List<ChatElement> messages = new ArrayList<>();
    public static ListView chatList;
    private Button sendButton;
    private EditText textField;
    private String currentUser = "DEFAULT";
    public static String currentChat;
    public static int chatOption = 0;
    public static ChatAdapter chatAdapter;

    private static FirebaseDatabase database = FirebaseDatabase.getInstance();
    private static DatabaseReference dbRef = database.getReference();
    private static DatabaseReference dbChat = dbRef.child("chat");

    private Handler pushHandler;
    private Runnable timedTask;
    private static boolean newMessageReceived = false;
    private static ChatElement lastMessage;

    //he fragment argument representing the section number for this fragment.
    private static final String ARG_SECTION_NUMBER = "section_number";

    //Returns a new instance of this fragment for the given section number.
    public static ChatFragment newInstance(int sectionNumber) {
        ChatFragment fragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        sendButton = rootView.findViewById(R.id.sendButton);
        textField = rootView.findViewById(R.id.textField);

        chatList = rootView.findViewById(R.id.chatList);
        chatAdapter = new ChatAdapter(rootView.getContext(), messages);
        chatList.setAdapter(chatAdapter);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textField.getText().length() > 0) {
                    ChatElement chatElement = new ChatElement(
                            new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()),
                            //DateFormat.getDateTimeInstance().format(new Date()),
                            currentUser,
                            textField.getText().toString()
                    );
                    textField.setText("");
                    //messages.add(chatElement);
                    //chatAdapter.notifyDataSetChanged();

                    dbChat.push().setValue(chatElement);
                }
            }
        });

        //Notifications
        pushHandler = new Handler();
        timedTask = new Runnable()
        {
            @Override
            public void run()
            {
                new NotificationManager().execute();
                pushHandler.postDelayed(timedTask, 1000 * 60);
            }
        };
        pushHandler.post(timedTask);

        return rootView;
    }

    @Override
    public void onStart(){
        super.onStart();

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        if (prefs.getString("Username","").equals(""))
            startActivityForResult(new Intent(getContext(), Options.class), 1);
        else
            currentUser = prefs.getString("Username","");

        connectToDatabase();
    }


    public static class ChatElement {
        public String date;
        public String user;
        public String message;

        ChatElement(){}

        ChatElement (String d, String u, String m){
            date = d;
            user = u;
            message = m;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("", "onActivityResult: Received");
        if (requestCode == 1) {
            Log.d("", "onActivityResult: RequestCode found");
            if (resultCode == 1) {
                Log.d("", "onActivityResult: ResultCode found");
                currentUser = data.getStringExtra("Username");
            }
        }
    }

    public static boolean isDuplicate(ChatElement ce) {
        for (int i=0; i<messages.size(); i++){
            if (ce.user.matches(messages.get(i).user)
                    && ce.message.matches(messages.get(i).message)
                    && ce.date.matches(messages.get(i).date)) {
                return true;
            }
        }

        return false;
    }

    public static void connectToDatabase() {
        dbChat.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                ChatElement ce = dataSnapshot.getValue(ChatElement.class);
                if (ce != null) {
                    if (!isDuplicate(ce)) {
                        messages.add(ce);
                        chatAdapter.notifyDataSetChanged();
                        newMessageReceived = true;
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {}
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {}
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {}
            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }


    //Notification Manager
    public class NotificationManager extends AsyncTask<Integer, Integer, Exception> {
        Exception exception = null;

        @Override
        protected Exception doInBackground(Integer... integers) {
            if (newMessageReceived) {
                notification();
                newMessageReceived = false;
            }
            return exception;
        }
    }

    private void notification() {
        if (getContext() == null)
            return;
        lastMessage = messages.get(messages.size()-1);
        Intent intent = new Intent(getContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(getActivity(), 0, intent, 0);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getContext());

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getContext(), "Lab04")
                .setSmallIcon(R.drawable.googleg_standard_color_18)
                .setContentTitle(lastMessage.user)
                .setContentText(lastMessage.message)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        notificationManager.notify(0, builder.build());
    }
}

