package imt3673.haakon.lab04;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Options extends AppCompatActivity {

    private Button sendButton;
    private EditText textField;
    private Button randomizeButton;

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference dbRef = database.getReference();
    private DatabaseReference dbUsers = dbRef.child("users");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);

        sendButton = findViewById(R.id.sendButton);
        textField = findViewById(R.id.textField);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (textField.getText().length() > 0) {
                    saveSettings();
                    dbUsers.push().setValue(textField.getText().toString());
                    finishActivity(0);
                }
            }
        });

        RandomString randomString = new RandomString(5);
        textField.setText(randomString.nextString());
    }

    private void saveSettings() {
        //Saved options
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();

        String user = textField.getText().toString();
        editor.putString("Username", user);

        //Apply changes
        editor.apply();

        Intent intent = new Intent();
        intent.putExtra("Username", user);
        setResult(1, intent);
        finish();
    }

    @Override
    public void onBackPressed(){

    }
}
