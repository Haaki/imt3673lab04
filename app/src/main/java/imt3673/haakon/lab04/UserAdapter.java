package imt3673.haakon.lab04;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class UserAdapter extends BaseAdapter {
    private List<String> textViews;
    public List<Integer> colors;
    private LayoutInflater inflater;

    public UserAdapter(Context applicationContext, List<String> textViews) {
        this.textViews = textViews;
        colors = new ArrayList<>();
        inflater = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return textViews.size();
    }

    @Override
    public Object getItem(int i) {
        return textViews.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.text_element, null);
        TextView user = view.findViewById(R.id.textBox);
        user.setText(textViews.get(i));
        if (colors.size() == i)
            colors.add(0);
        view.setBackgroundColor(colors.get(i));
        return view;
    }
}
